<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class ImageTest extends TestCase
{
    /**
     * Test generated urls for resized images
     *
     * @return void
     */
    public function testImageResizedSource()
    {
        $images = factory(App\Models\Image::class, 15)->make();
        $envImageSizes = explode(',', env('IMAGE_SIZES'));

        $this->assertNotEmpty($envImageSizes);

        $images->each(function ($image) use ($envImageSizes) {
            $filePath = substr($image->image_source, 0, -4);
            $extension = substr($image->image_source, -3);
            $testExpected = [];

            foreach ($envImageSizes as $size) {
                $testExpected[$size] = implode('.', [$filePath, $size, $extension]);
            }

            $this->assertSame($testExpected, $image->resized_sources->toArray());
        });
    }
}
