<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;
use App\Models\Site;

class ArticleApiTest extends TestCase
{
    use DatabaseMigrations;
    
    public function setUp()
    {
        parent::setUp();

        $this->artisan('db:seed-sample', ['--sql' => dirname(__FILE__) . '/data/data.sql']);
    }

    /**
     * Test list of articles
     *
     * @return void
     */
    public function testArticlesAreListedCorrectly()
    {
        $site = Site::first();

        $this->json('get', "/api/articles/{$site->site_id}")
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    [
                        'article_id',
                        'created_date',
                        'heading',
                        'sub_heading',
                        'preabmle',
                        'relative_url',
                        'site_id',
                        'published_date',
                        'images' => [],
                    ]
                ],
            ]);
    }

    /**
     * Test list of articles
     *
     * @return void
     */
    public function testArticlesAreOrderedCorrectly()
    {
        $site = Site::find(64);

        $this->json('get', "/api/articles/{$site->site_id}");
        $articles = $this->response->decodeResponseJson()['data'];

        for ($i = 1; $i < count($articles); $i++) {
            $this->assertGreaterThanOrEqual(
                $articles[$i]['published_date'],
                $articles[$i - 1]['published_date']
            );
        }
    }

    /**
     * Test article
     *
     * @return void
     */
    public function testsArticlesAreDisplayedCorrectly()
    {
        $site = Site::first();

        collect([449654, 449566, 449527])
            ->each(function ($articleId) use ($site) {
                $this->json('get', "/api/articles/{$site->site_id}/{$articleId}")
                    ->assertStatus(200)
                    ->assertJsonStructure([
                        'data' => [
                            'article_id',
                            'created_date',
                            'heading',
                            'sub_heading',
                            'preabmle',
                            'relative_url',
                            'site_id',
                            'published_date',
                            'images' => [],
                            'authors' => [],
                            'content' => []
                        ],
                    ]);
            });
    }
}
