
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('navigation', require('./components/Navigation.vue'));
Vue.component('newsfeed', require('./components/NewsFeed.vue'));
Vue.component('article-preview', require('./components/ArticlePreview.vue'));
Vue.component('article-component', require('./components/Article.vue'));

let store = {
    currentNewsfeed: {
        site_id: null,
        article_id: null,
    },

    goTo (site_id, article_id = '') {
        article_id = article_id.replace(/^\//, '');
        this.currentNewsfeed = {site_id, article_id};

        history.pushState({urlPath: `/${site_id}/${article_id}`}, '', `/${site_id}/${article_id}`)
    },

};

const app = new Vue({
    el: '#app',
    data: store
});