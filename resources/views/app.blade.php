<!DOCTYPE html>
<html lang="{{ config('app.locale', 'en') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'App') }}</title>

    <!-- Scripts -->
    <script src="/js/app.js" defer></script>

    <!-- Styles -->
    <link href="/css/app.css" rel="stylesheet">
</head>
<body>
    <div id="app">
        <div class="container">
            <header class="newsfeed-header py-3">
                <div class="row flex-nowrap justify-content align-items-center">
                    <div class="col text-center">
                        <a class="newsfeed-logo text-dark" href="/">{{ config('app.name', 'App') }}</a>
                    </div>
                </div>
            </header>

            <newsfeed :sites="{{ json_encode($sites) }}"></newsfeed>
        </div>
    </div>
</body>
</html>
