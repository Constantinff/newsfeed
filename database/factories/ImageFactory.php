<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Image::class, function (Faker $faker) {
    return [
        'image_id' => $faker->unique()->randomNumber,
        'article_id' => $faker->randomNumber,
        'image_source' => $faker->imageUrl() . '/' .
            $faker->regexify('[A-Za-z0-9_.-~]{2,30}') . '.' .
            $faker->randomElement(['jpg', 'png', 'gif']),
    ];
});
