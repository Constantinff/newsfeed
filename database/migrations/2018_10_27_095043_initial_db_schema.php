<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDbSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $rawSchemaFile = file_get_contents(dirname(__FILE__) . '/sql/schema.sql');
        $results = DB::unprepared($rawSchemaFile);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();

        Schema::dropIfExists('site');
        Schema::dropIfExists('articles');
        Schema::dropIfExists('authors');
        Schema::dropIfExists('article_authors');
        Schema::dropIfExists('article_content');
        Schema::dropIfExists('article_images');
        
        Schema::enableForeignKeyConstraints();
    }
}
