<?php

use App\Models\Article;
use App\Models\Site;
use App\Http\Resources\Article as ArticleResource;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['prefix' => 'api'], function ($router) {
    $router->group(['prefix' => 'articles'], function ($router) {
        Route::get('/{siteId}', function ($siteId) {
            $siteArticles = Article::where('site_id', $siteId)
                ->with(['images'])
                ->orderBy('published_date', 'desc')
                ->get();

            return new ArticleResource($siteArticles);
        });

        Route::get('/{siteId}/{id:.*}', function ($siteId, $id) {
            if (!is_numeric($id)) {
                $path = explode('/', $id);
                $id = array_pop($path);
            }

            $article = Article::where('article_id', $id)
                ->with(['images', 'authors', 'content'])
                ->firstOrFail()
                ->append('similar');

            return new ArticleResource($article);
        });
    });
});

$router->get('/{route:.*}/', function () {
    return view('app')
        ->with('sites', Site::all());
});
