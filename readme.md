NewsFeed with Lumen PHP Framework 
==========
NewsFeed built with Laravel Lumen and VueJS frontend

## Requirements
* php >= 7.1.3
* NodeJS >= 8

## Setup

### Clone the project
```
git clone git@gitlab.com:Constantinff/newsfeed.git
```

### Install composer dependencies
```
composer install
```
for production use `--no-dev` flag

### Install npm dependencies
```
npm install
```

### Build css/js
```
npm run development
```
or for prod
```
npm run production
```

### Setup env
```
composer run post-root-package-install
```
and edit information in `.env` file

### Setup database
and change `.env` accordingly

Run database migrations
```
php artisan migrate
```
Also you can seed database with data from .sql file
```
php artisan db:seed-sample --sql='/path/to/your/sample/data.sql'
```

### Run NewsFeed for development mode
```
php -S localhost:8000 -t public
```

## Documentation
### Scaled images
For additional scaled and resized images
edit `.env` file and change `IMAGE_SIZES` property
with comma separated scale options

and get all scaled image urls with
`$image->resized_sources`

### Used frameworks/libs
* Lumen PHP Framework (https://lumen.laravel.com/)
* Vue.js Frontend Framework (https://vuejs.org/)
* Laravel-mix webpack preprocessor for building js/css (https://laravel-mix.com)
* Axios (https://github.com/axios/axios)
* Bootstrap4 *(styles only)* (https://getbootstrap.com/)
* Lodash (https://lodash.com)
* Moment (https://momentjs.com/)
* PHPUnit for testing
* Lumen Testing - extended Lumen assertions (https://github.com/albertcht/lumen-testing)

## Testing
A database with name `testing` is required

And to run the PHPUnit tests
```
composer test
```