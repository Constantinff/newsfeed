<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    // set database table
    protected $table = 'article_images';

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['resized_sources'];

    /**
     * Get resized images.
     *
     * @return Collection object
     */
    public function getResizedSourcesAttribute()
    {
        $imageSizes = collect(explode(',', env('IMAGE_SIZES')));
        $sourcePath = pathinfo($this->image_source);

        return $imageSizes->mapWithKeys(function ($size) use ($sourcePath) {
            return [
                $size => $sourcePath['dirname'] . '/' . $sourcePath['filename'] . '.' .
                    $size . '.' . $sourcePath['extension']
            ];
        });
    }
}
