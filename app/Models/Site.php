<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    // set database table
    protected $table = 'site';

    // set default primary key
    protected $primaryKey = 'site_id';

    /**
     * Get the site articles.
     */
    public function articles()
    {
        return $this->hasMany('App\Models\Article', 'site_id');
    }
}
