<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    // set default primary key
    protected $primaryKey = 'author_id';
}
