<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // set default primary key
    protected $primaryKey = 'article_id';
    
    /**
     * Get the content for the article.
     */
    public function content()
    {
        return $this->hasOne('App\Models\Content', 'article_id');
    }
    
    /**
     * Get the images for the article.
     */
    public function images()
    {
        return $this->hasMany('App\Models\Image', 'article_id');
    }

    /**
     * The article authors
     */
    public function authors()
    {
        return $this->belongsToMany('App\Models\Author', 'article_authors', 'article_id', 'author_id');
    }

    /**
     * Get similar Articles.
     *
     * @return Collection object
     */
    public function getSimilarAttribute()
    {
        if (empty($this->sub_heading)) {
            return [];
        }

        return self::where('sub_heading', $this->sub_heading)
            ->where('article_id', '!=', $this->article_id)
            ->with(['images'])
            ->get();
    }
}
