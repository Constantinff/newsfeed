<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SeedSampleData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:seed-sample {--sql=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seeds database with sample data from .sql file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sqlFilePath = $this->option('sql');

        if (!file_exists($sqlFilePath)) {
            throw new \Exception("Unable for find sql file: " . $sqlFilePath);
        }

        $rawSchemaFile = file_get_contents($sqlFilePath);
        $results = \DB::unprepared($rawSchemaFile);
    }
}
